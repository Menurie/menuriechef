//
//  LeftImageTextfield.swift
//  Menurie
//
//  Created by apple on 6/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class LeftImageTextfield: UITextField {

    @IBInspectable var leftImage: UIImage?{
        didSet{
            updateView()
        }
    }
    @IBInspectable var leftPadding: CGFloat = 0{
        didSet{
            updateView()
        }
    }
    func updateView(){
        if let image = leftImage{
            leftViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 3, width: 14, height: 14))
            imageView.image = image
            var width = leftPadding + 15
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line {
                width = width + 5
            }
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            view.addSubview(imageView)
            leftView = view
        } else{
            leftViewMode = .never
        }
    }

}
