//
//  PhotosVC.swift
//  Menurie
//
//  Created by apple on 6/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class PhotosVC: UIViewController {

    //MARK:- Outlets
    
    //MARK:- Variables
    
    //MARK:- Array
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Actions
    
    //MARL:- Functions

}
extension PhotosVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 14
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotosCVCell", for: indexPath) as! PhotosCVCell
        if indexPath.row == 0 {
            cell.photosBtn.isHidden = false
            cell.photoos.isHidden = true
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/4, height: collectionView.frame.width/4)
    }
}
