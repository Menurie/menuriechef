//
//  LoginVC.swift
//  Menurie
//
//  Created by apple on 6/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var emailTF: LeftImageTextfield!
    @IBOutlet weak var passwordTF: LeftImageTextfield!
    
    //MARK:- Variables
    
    //MARK:- Array
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- Actions
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func createClicked(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func forgetClicked(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ResetVC") as! ResetVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func loginClicked(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARL:- Functions

}
