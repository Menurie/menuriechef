//
//  ViewController.swift
//  Menurie
//
//  Created by apple on 6/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    //MARK:- Outlet
    
    //MARK:- Variables
    
    //MARK:- Arrays
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    //MARK:- Actions
    @IBAction func fbBtnClicked(_ sender: UIButton) {
    }
    
    @IBAction func googleBtnClicked(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
        navigationController.setViewControllers([storyboard.instantiateViewController(withIdentifier: "ViewController")], animated: false)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "mai") as! MainViewController
        mainViewController.rootViewController = navigationController
        mainViewController.setup(type: UInt(0))
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
        
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        
    }
    
    @IBAction func loginInNowClicked(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Functions
    
    
}

