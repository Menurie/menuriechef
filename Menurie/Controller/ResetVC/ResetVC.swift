//
//  ResetVC.swift
//  Menurie
//
//  Created by apple on 6/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ResetVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var emailTF: LeftImageTextfield!
    
    //MARK:- Variables
    
    //MARK:- Array
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Actions
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resendClicked(_ sender: UIButton) {
    }
    @IBAction func resetNowClicked(_ sender: UIButton) {
    }
    
    //MARL:- Functions

}
