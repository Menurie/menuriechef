//
//  AddItemsVC.swift
//  Menurie
//
//  Created by apple on 6/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class AddItemsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var fastFoodTF: LeftImageTextfield!
    @IBOutlet weak var itemNameTF: LeftImageTextfield!
    @IBOutlet weak var descriptionTV: UITextView!
    @IBOutlet weak var priceTF: LeftImageTextfield!
    
    //MARK:- Variables
    
    //MARK:- Array
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        descriptionTV.layer.borderWidth = 0.5
        descriptionTV.layer.cornerRadius = 6
        descriptionTV.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
    
    //MARK:- Actions
    
    //MARL:- Functions

}
