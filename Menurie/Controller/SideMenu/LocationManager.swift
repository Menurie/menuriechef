////
////  LocationManager.swift
////  SmartWashr
////
////  Created by Rizwan on 7/6/18.
////  Copyright © 2018 Branchez Consulting. All rights reserved.
////
//
//import UIKit
//import GoogleMaps
//import CoreLocation
//
//
//class LocationManager: NSObject,CLLocationManagerDelegate {
//    
//    static let sharedInstance = LocationManager()
//    
//    typealias addressCompletion = (CLPlacemark?, String?) -> Void
//    
//    
//    var locationManager: CLLocationManager?
//    var currentLocation: CLLocation?
//    var placemarkObj: CLPlacemark?
//    var addressSt = ""
//    
//    
//    
//    
//    
//    override init() {
//        super.init()
//        
//        locationManager = CLLocationManager()
//        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager?.distanceFilter = CLLocationDistance(100)
//        // meters
//        locationManager?.delegate = self
//        if (locationManager?.responds(to: #selector(locationManager?.requestWhenInUseAuthorization)))! {
//            locationManager?.requestWhenInUseAuthorization()
//        }
//    }
//    
//    
//    func checkLocationPermissions(controller:UIViewController)->Bool{
//        if CLLocationManager.locationServicesEnabled(){
//            switch CLLocationManager.authorizationStatus(){
//                
//            case .notDetermined:
//                print("Not determined")
//                Utilities.showAlertWithTitleForLocation(title: settingObject.translations.locationNotDeterminedLabel, withMessage: settingObject.translations.weRequireAccessToYourLocationLabel, withNavigation: controller)
//                return false
//            case .restricted:
//                print("Restricted")
//                Utilities.showAlertWithTitleForLocation(title: settingObject.translations.locationRestrictedLabel, withMessage: settingObject.translations.weRequireAccessToYourLocationLabel, withNavigation: controller)
//                return false
//            case .denied:
//                Utilities.showAlertWithTitleForLocation(title: settingObject.translations.locationDeniedLabel, withMessage: settingObject.translations.weRequireAccessToYourLocationLabel, withNavigation: controller)
//                return false
//            case .authorizedAlways:
//                return true
//            case .authorizedWhenInUse:
//                return true
//                
//            }
//            
//        }else{
//            Utilities.showAlertWithTitle(title: settingObject.translations.locationServiceLabel, withMessage: settingObject.translations.enableLocationDescriptionLabel, withNavigation: controller)
//            return false
//        }
//        
//        //        return false
//    }
//    
//    func startUpdatingLocation() {
//        print("Starting location updates")
//        locationManager?.startUpdatingLocation()
//    }
//    
//    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//        print("Location service failed with error %@", error)
//    }
//    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        
//        
//        
//        let location = locations.last
//        print(String(format: "Latitude %+.6f, Longitude %+.6f\n", (location?.coordinate.latitude)!, (location?.coordinate.longitude)!))
//        currentLocation = location
//        getAddressFrom(currentLocation, complationBlock: { placeMarks, address in
//            self.placemarkObj = placeMarks
//            self.addressSt = address!
//            print(self.currentLocation ?? 0)
//            //                self.setValue(self.currentLocation, forKey: "currentLocation")
//        })
//        let nc = NotificationCenter.default
//        nc.post(name: Notification.Name("locationget"), object: nil)
//        
//        
//        
//    }
//    
//    func getAddressFrom(_ location: CLLocation?, complationBlock completionBlock: @escaping addressCompletion) {
//        var placemark: CLPlacemark?
//        var address: String? = nil
//        currentLocation = location
//        let geocoder = CLGeocoder()
//        if let aLocation = location {
//            geocoder.reverseGeocodeLocation(aLocation, completionHandler: { placemarks, error in
//                if error == nil && (placemarks?.count ?? 0) > 0 {
//                    print(placemark)
//                    
//                    placemark = placemarks?.first
//                    address = "\(placemark?.name ?? ""), \(placemark?.postalCode ?? "") \(placemark?.locality ?? "")"
//                    print(address)
//                    completionBlock(placemark, address)
//                }
//            })
//        }
//    }
//    
//    
//    
//    func getAddressFromLocation(_ location: CLLocation?, complationBlock completionBlock: @escaping addressCompletion) {
//        
//        let geocoder = GMSGeocoder()
//        
//        geocoder.reverseGeocodeCoordinate((location?.coordinate)!) { (response, error) in
//            if let address = response?.firstResult() {
//                let lines = address.lines! as [String]
//                completionBlock(nil, lines.joined(separator: ","))
//            }
//        }
//    }
//    
//    
//    
//    func getAddress(_ location: CLLocation?, complationBlock completionBlock: @escaping addressCompletion)  {
//        var address: String = ""
//        
//        let geoCoder = CLGeocoder()
//        let location = CLLocation(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
//        //selectedLat and selectedLon are double values set by the app in a previous process
//        
//        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
//            
//            // Place details
//            var placeMark: CLPlacemark!
//            placeMark = placemarks?[0]
//            
//            
//            // Address dictionary
//            //print(placeMark.addressDictionary ?? "")
//            
//            // Location name
//            if let locationName = placeMark.addressDictionary!["Name"] as? String {
//                print(locationName)
//                address = address + locationName + " "
//            }
//            
//            // Street address
//            if let street = placeMark.addressDictionary!["Thoroughfare"] as? String {
//                print(street)
//                //                address = address + street + " "
//            }
//            
//            // City
//            if let city = placeMark.addressDictionary!["City"] as? String {
//                print(city)
//                address = address + city + " "
//            }
//            
//            // Zip code
//            if let zip = placeMark.addressDictionary!["ZIP"] as? String {
//                //print(zip)
//            }
//            
//            // Country
//            if let country = placeMark.addressDictionary!["Country"] as? String {
//                print(country)
//                address = address + country + " "
//            }
//            print(address)
//            completionBlock(placeMark , address)
//        })
//        
//        
//    }
//    
//    /*
//     // MARK: - Navigation
//     
//     // In a storyboard-based application, you will often want to do a little preparation before navigation
//     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//     // Get the new view controller using segue.destinationViewController.
//     // Pass the selected object to the new view controller.
//     }
//     */
//    
//    
//    
//}
