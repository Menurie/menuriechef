//
//  MainViewController.swift
//  LGSideMenuControllerDemo
//

import UIKit
import LGSideMenuController

class MainViewController: LGSideMenuController {

    private var type: UInt?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.leftViewBackgroundColor = #colorLiteral(red: 0.9664065242, green: 0.7420412898, blue: 0, alpha: 1)
        self.view.backgroundColor = #colorLiteral(red: 0.9038441777, green: 0.6588760018, blue: 0.04511878639, alpha: 1)
        
    }
    
    func setup(type: UInt) {
        self.type = type

        if (self.storyboard != nil) {
            
            
        }
        else {
            leftViewController = LeftMenuViewController()
            
         //   self.view.backgroundColor = #colorLiteral(red: 0.9038441777, green: 0.6588760018, blue: 0.04511878639, alpha: 1)
            leftViewWidth = 300.0;
            //leftViewBackgroundImage = UIImage(named: "imageLeft")
            //leftViewBackgroundColor = #colorLiteral(red: 0.9664408565, green: 0.742333591, blue: 0, alpha: 1)
          //  rootViewCoverColorForLeftView = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 0.05)

        }
    
        let greenCoverColor = #colorLiteral(red: 0.9038441777, green: 0.6588760018, blue: 0.04511878639, alpha: 1)
        let purpleCoverColor = #colorLiteral(red: 0.9038441777, green: 0.6588760018, blue: 0.04511878639, alpha: 1)
        let regularStyle: UIBlurEffect.Style

        if #available(iOS 10.0, *) {
            regularStyle = .regular
        }
        else {
            regularStyle = .prominent
        }

        // -----

        switch type {
        case 0:
            leftViewPresentationStyle = .scaleFromBig
           
            break
   
        default:
            break
        }
    }

    override func leftViewWillLayoutSubviews(with size: CGSize) {
        super.leftViewWillLayoutSubviews(with: size)

        if !isLeftViewStatusBarHidden {
            leftView?.frame = CGRect(x: 0.0, y: 20.0, width: size.width, height: size.height - 20.0)
        }
    }
    
    override var isLeftViewStatusBarHidden: Bool {
        get {
            return super.isLeftViewStatusBarHidden
        }
        set {
            super.isLeftViewStatusBarHidden = newValue
        }
    }

}
