//
//  HomeVC.swift
//  Remote Barber (User)
//
//  Created by Muhammad Zunair on 09/02/2020.
//  Copyright © 2020 Muhammad Zunair. All rights reserved.
//

import UIKit

class HomeVC: UIViewController{
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       let menu   = UIBarButtonItem(image: #imageLiteral(resourceName: "add_ic"),  style: .plain, target: self, action: #selector(HomeVC.menu))
        
      //  let chart = UIBarButtonItem(image: chartImg,  style: .plain, target: self, action: #selector(HomeVC.chart))
       // let filter = UIBarButtonItem(image: filterImg,  style: .plain, target: self, action: #selector(HomeVC.filter))
        // let notification = UIBarButtonItem(image: #imageLiteral(resourceName: "bell-1"),  style: .plain, target: self, action: #selector(HomeViewController.notifi))
        
        navigationItem.leftBarButtonItem = menu
        //navigationItem.rightBarButtonItems = [filter, chart]
    }
    
    @objc func menu(){
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
    }
    
    
}
