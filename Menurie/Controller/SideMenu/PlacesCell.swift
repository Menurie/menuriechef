//
//  PlacesCell.swift
//  GoGrub
//
//  Created by Muhammad Zunair on 02/12/2019.
//  Copyright © 2019 apple. All rights reserved.
//


import UIKit

class PlacesCell: UITableViewCell {
    
    @IBOutlet weak var forwardButton: UIImageView!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeAddressLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
