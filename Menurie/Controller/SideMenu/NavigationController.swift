//
//  NavigationController.swift
//  LGSideMenuControllerDemo
//

import UIKit
import LGSideMenuController

class NavigationController: UINavigationController {

    override var shouldAutorotate : Bool {
        return true
    }
    

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
 
}
