//
//  SuccessFlile.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 08/11/2019.
//  Copyright © 2019 apple. All rights reserved.
//


import Foundation
import UIKit

protocol Datapass {
    func dataPass()
}

class SuccessVu: UIView {
    

     var delegates : Datapass!
    static let instance = SuccessVu()
    
   // @IBOutlet var parentView: UIView!
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var alertVuew: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var titless: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("SuccessVu", owner: self, options: nil)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        img.layer.cornerRadius = 30
        img.layer.borderColor = UIColor.white.cgColor
        img.layer.borderWidth = 2
        
        alertVuew.layer.cornerRadius = 15
        doneBtn.layer.cornerRadius = 8.0
        
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    enum AlertType {
        case success
        case failure
    }
    
    func showAlert(title: String, message: String, alertType: AlertType) {
        
        Bundle.main.loadNibNamed("SuccessVu", owner: self, options: nil)
        commonInit()
        
        
        self.titless.text = title
        self.message.text = message
        
        switch alertType {
        case .success:
      //      img.image = #imageLiteral(resourceName: "FO")
            doneBtn.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        case .failure:
           // check = 1
        //    img.image = #imageLiteral(resourceName: "RemoteAccess")
            doneBtn.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }
    
    @IBAction func onClickDone(_ sender: Any) {
        
       // if check != 1{
             self.delegates.dataPass()
       // }else{
        //    check = 0
      //  }
        parentView.removeFromSuperview()
     
        
    }
    
    
    
    
    
    
    
}
