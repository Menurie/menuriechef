//
//  LeftMenuViewController.swift
//  GoGrub
//
//  Created by Muhammad Zunair on 10/11/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController {

    
    //Mark :- Variables
    
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var btn: UIButton!
    
    private let buyerMenuArr = ["Order Now","Profile","Order History","Cart","Settings"]
        private let chefMenuArr = ["My Post","Post Shop","Notifications","Inbox","Manage Order","Earnings","Setting"]
    
    let menuIconsBuyer = [#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo")]
    let menuIconschef = [#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo")]
    
    
    
    @IBOutlet weak var tblVu: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
//        if !is_chef{
//            btn.setTitle("Become A Barber", for: UIControl.State.normal)
//          // btn.setTitle("View as chef", for: UIControl.State.normal)
//        }else{
//            if isBuyerActive{
//                btn.setTitle("View as Barber", for: UIControl.State.normal)
//            }else{
//                btn.setTitle("View as Buyer", for: UIControl.State.normal)
//            }

       // }
        // Do any additional setup after loading the view.
    }
    
   // @IBAction func tappedBtn(_ sender: Any) {
//        if !is_chef{
//
//                      // let vc = self.storyboard?.instantiateViewController(withIdentifier: "map") as! SelectAddressController
//                     //   self.present(vc, animated: true, completion: nil)
//
//        }else{
//
//            if isBuyerActive{
//
//                isBuyerActive = false
//                sideMenuController?.showLeftView(animated: true, completionHandler: nil)
//                btn.setTitle("View as chef", for: UIControl.State.normal)
//                tblVu.reloadData()
//
//                homeScreen()
//
//            }else{
//                isBuyerActive = true
//                sideMenuController?.showLeftView(animated: true, completionHandler: nil)
//                btn.setTitle("View as Buyer", for: UIControl.State.normal)
//                tblVu.reloadData()
//                homeScreen()
//
//            }

        //}
        
   // }


    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }
    
  

}

extension LeftMenuViewController : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var value = buyerMenuArr.count
//        if !isBuyerActive{
//            value = chefMenuArr.count
//        }
        return value
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LeftViewCell
        
        
//        if isBuyerActive{
//            cell.titleLabel.text = buyerMenuArr[indexPath.row]
//            cell.img.image = menuIconsBuyer[indexPath.row]
//        }else{
//            cell.img.image = menuIconschef[indexPath.row]
//            cell.titleLabel.text = chefMenuArr[indexPath.row]
//        }
//
        
        
        
        return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        if isBuyerActive{
//
//            switch indexPath.row{
//            case 0:
//                homeScreen()
//                break
//            case 1:
//                self.performSegue(withIdentifier: "profile", sender: self)
//
//                break
//            case 2:
//                self.performSegue(withIdentifier: "orderHistory", sender: self)
//                break
//            case 3:
////                let cart = self.storyboard?.instantiateViewController(withIdentifier: "cart") as! orderSetViewController
//             //   self.present(cart, animated: true, completion: nil)
//
//                break
//            case 4:
//                self.performSegue(withIdentifier: "setting", sender: self)
//                break
//
//            default:
//                return
//            }
//
//        }else{
            
            switch indexPath.row{
            case 0:
                //homeScreen()
                break
            case 1:
                self.performSegue(withIdentifier: "addFood", sender: self)
                
                break
            case 2:
             //   let vc = self.storyboard?.instantiateViewController(withIdentifier: "notification") as! NotificationViewController
               // self.present(vc, animated: true, completion: nil)
                break
            case 3:
                
                self.performSegue(withIdentifier: "inbox", sender: self)
                break
            case 4:
                self.performSegue(withIdentifier: "manageOrder", sender: self)
                break
            case 5:
                self.performSegue(withIdentifier: "earning", sender: self)
                
                break
            case 6:
                self.performSegue(withIdentifier: "setting", sender: self)
                break
                
                
            default:
                return
            }
      //  }
      
    
    }
}
