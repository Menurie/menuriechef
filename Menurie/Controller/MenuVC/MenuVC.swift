//
//  MenuVC.swift
//  Menurie
//
//  Created by apple on 6/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SwipeMenuViewController

class MenuVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var SwipeView: SwipeMenuView!
    
    //MARK:- Variables
    var options: SwipeMenuViewOptions!
    var names: [String] = ["Categories","Items", "Add ons"]
    var ViewControllers:[UIViewController] = [UIViewController()]
    
    //MARK:- Arrays
    override func viewDidLoad() {
        super.viewDidLoad()
        
        options = .init()
        configMenu()
        super.viewDidLoad()
        SwipeView.reloadData(options: options)
    }
    
    //MARK:- Actions
    
    //MARK:- Functions
    func configMenu(){
        SwipeView.dataSource = self
        ViewControllers.removeAll()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let Categories = storyboard.instantiateViewController(withIdentifier: "CategoriesVC")
        let Items = storyboard.instantiateViewController(withIdentifier: "ItemsVC")
        let AddOns = storyboard.instantiateViewController(withIdentifier: "AddOnsVC")
        ViewControllers = [Categories, Items, AddOns]
        options.tabView.style = .segmented
        //options.tabView.itemView.font = UIFont(name: "Roboto-Bold", size: 13)!
        options.tabView.addition = .underline
        options.tabView.additionView.backgroundColor = #colorLiteral(red: 0.5529411765, green: 0.3294117647, blue: 0.2509803922, alpha: 1)
        options.tabView.itemView.selectedTextColor = #colorLiteral(red: 0.5529411765, green: 0.3294117647, blue: 0.2509803922, alpha: 1)
    }
    
}
//MARK: — Swipe Delegate
extension MenuVC:SwipeMenuViewDataSource{
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
        return ViewControllers[index]
        
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        return names[index]
    }
    
    func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        return ViewControllers.count
    }
}
