//
//  ItemsVC.swift
//  Menurie
//
//  Created by apple on 6/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ItemsVC: UIViewController {

    //MARK:- Outlets
    
    //MARK:- Variables
    
    //MARK:- Array
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Actions
    
    //MARL:- Functions
    
}

extension ItemsVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTVCell", for: indexPath) as! ItemTVCell
        return cell
    }
}
