//
//  SignupVC.swift
//  Menurie
//
//  Created by apple on 6/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var fNameTF: LeftImageTextfield!
    @IBOutlet weak var lNameTF: LeftImageTextfield!
    @IBOutlet weak var emailTF: LeftImageTextfield!
    @IBOutlet weak var numberTF: LeftImageTextfield!
    @IBOutlet weak var passwordTF: LeftImageTextfield!
    @IBOutlet weak var confirmPasswordTF: LeftImageTextfield!
    
    //MARK:- Variables
    
    //MARK:- Array
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Actions
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func registerClicked(_ sender: UIButton) {
    }
    
    //MARL:- Functions
    
}
