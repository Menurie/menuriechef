//
//  MyOrderVC.swift
//  Menurie
//
//  Created by apple on 6/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class MyOrderVC: UIViewController {

    //MARK:- Outlets
    
    //MARK:- Variables
    
    //MARK:- Array
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Actions
    
    //MARL:- Functions

}
extension MyOrderVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderTVCell", for: indexPath) as! MyOrderTVCell
        return cell
    }
}
