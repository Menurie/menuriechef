//
//  TodaysOrderVC.swift
//  Menurie
//
//  Created by apple on 6/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class TodaysOrderVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tbViewHeight: NSLayoutConstraint!
    
    //MARK:- Variables
    
    //MARK:- Array
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tbViewHeight.constant = 100 * 10
    }
    
    //MARK:- Actions
    
    //MARL:- Functions


}
extension TodaysOrderVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodaysOrderTVCell", for: indexPath) as! TodaysOrderTVCell
        return cell
    }
}
