//
//  DashboardVC.swift
//  Menurie
//
//  Created by apple on 6/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SwipeMenuViewController

class DashboardVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var SwipeView: SwipeMenuView!
    
    //MARK:- Variables
    var options: SwipeMenuViewOptions!
    var names: [String] = ["Today","Week", "Custom"]
    var ViewControllers:[UIViewController] = [UIViewController()]
    
    //MARK:- Arrays
    override func viewDidLoad() {
        super.viewDidLoad()
        
        options = .init()
        configMenu()
        super.viewDidLoad()
        SwipeView.reloadData(options: options)
    }
    
    //MARK:- Actions
    
    //MARK:- Functions
    func configMenu(){
        SwipeView.dataSource = self
        ViewControllers.removeAll()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let Today = storyboard.instantiateViewController(withIdentifier: "TodaysOrderVC")
        let Week = storyboard.instantiateViewController(withIdentifier: "WeeklyOrderVC")
        let Custom = storyboard.instantiateViewController(withIdentifier: "CustomVC")
        ViewControllers = [Today, Week, Custom]
        options.tabView.style = .segmented
        //
        //text1
        //options.tabView.itemView.font = UIFont(name: "Roboto-Bold", size: 13)!
        options.tabView.addition = .underline
        options.tabView.additionView.backgroundColor = #colorLiteral(red: 0.5529411765, green: 0.3294117647, blue: 0.2509803922, alpha: 1)
        options.tabView.itemView.selectedTextColor = #colorLiteral(red: 0.5529411765, green: 0.3294117647, blue: 0.2509803922, alpha: 1)
    }
    
}
//MARK: — Swipe Delegate
extension DashboardVC:SwipeMenuViewDataSource{
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
        return ViewControllers[index]
        
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        return names[index]
    }
    
    func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        return ViewControllers.count
    }
}
